package com.company;

import java.util.ArrayList;
import java.util.List;

public class Control extends Department {

  private final List<Department> departments;

  public Control(int id, String name) {
    super(id, name);
    this.departments = new ArrayList<>();
  }

  public void displayName() {
    System.out.println(this.name);
  }

  public void displayNames() {
    this.displayName();
    for (Department d : departments)
      d.displayName();
  }

  public void add(Department d) {
    this.departments.add(d);
  }

  public void remove(Department d) {
    departments.remove(d);
  }

  public Department getChild(int index) {
    return departments.get(index);
  }
}