package com.company;

public class Finance extends Department {

  public Finance(int id, String name) {
    super(id, name);
  }

  public void displayName() {
    System.out.println(this.name);
  }

}