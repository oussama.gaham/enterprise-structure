package com.company;

import java.util.Objects;

public abstract class Department {

  public Department(int id, String name){
    this.id = id;
    this.name = name;
  }

  public abstract void displayName();

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Department that = (Department) o;
    return id == that.id && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  protected int id;
  protected String name;

}