package com.company;

public class Management extends Department {

  public Management(int id, String name) {
    super(id, name);
  }

  public void displayName() {
    System.out.println(this.name);
  }

}