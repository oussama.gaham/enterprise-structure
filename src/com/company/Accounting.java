package com.company;

public class Accounting extends Department {
  @Override
  public void displayName() {
    System.out.println(this.name);
  }

  public Accounting(int id, String name) {
    super(id, name);
  }
}