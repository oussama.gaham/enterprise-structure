package com.main;

import com.company.Control;
import com.company.Department;
import com.company.Finance;
import com.company.Management;

public class Main {

  public static void main(String[] args) {
    Department finance = new Finance(1, "finance1");
    Department management = new Management(2, "gestion1");
    Control control = new Control(3, "control1");
    control.add(management);
    control.add(finance);
    control.displayNames();
  }
}
